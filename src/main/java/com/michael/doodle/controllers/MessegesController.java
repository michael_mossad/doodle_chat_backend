package com.michael.doodle.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.michael.doodle.eb.models.MessegeEB;
import com.michael.doodle.services.MessegesService;

@RestController
@RequestMapping("/messeges")
public class MessegesController {
	 @Autowired
	 MessegesService messegesService;
	 
	MessegesService keywordEvaluationService;
	@RequestMapping(value = "/get_messeges", method = RequestMethod.GET)
	public List<MessegeEB> getMesseges() {
		return messegesService.listAllUsers();
	}
	
	@RequestMapping(value = "/add_messeges", method = RequestMethod.POST)
	public void addMessege(@RequestBody MessegeEB messege) {
		messegesService.addMessege(messege);
	}
}
