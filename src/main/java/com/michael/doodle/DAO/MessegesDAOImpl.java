package com.michael.doodle.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.michael.doodle.eb.models.MessegeEB;

@Repository
public class MessegesDAOImpl implements MessegesDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MessegeEB> listAllMessegs() {
		Session session = this.sessionFactory.getCurrentSession();
		session.beginTransaction();
		List<MessegeEB> messegesList = session.createQuery("from MessegeEB order by date(createdOn) desc").list();
		session.close();
		return messegesList;
	}
	@Override
	public void addMessegeEB(MessegeEB messege) {
		Session session = this.sessionFactory.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.persist(messege);
		transaction.commit();
		
		System.out.println("MessegeEB saved successfully, MessegeEB Details="+messege);
	}

}
