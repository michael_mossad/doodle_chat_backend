package com.michael.doodle.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.michael.doodle.DAO.MessegesDAO;
import com.michael.doodle.eb.models.MessegeEB;

@Service
public class MessegesServiceImpl implements MessegesService {
 
 MessegesDAO userDao;
 
 @Autowired
 public void setUserDao(MessegesDAO userDao) {
  this.userDao = userDao;
 }

 public List<MessegeEB> listAllUsers() {
  return userDao.listAllMessegs();
 }

 public void addMessege(MessegeEB messege) {
  userDao.addMessegeEB(messege);
 }

}