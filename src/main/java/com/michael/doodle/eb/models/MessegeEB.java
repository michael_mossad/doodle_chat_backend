package com.michael.doodle.eb.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "messges")
public class MessegeEB  implements Serializable {
    private static final long serialVersionUID = 4865903039190150223L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	@Column(name="createdOn")
	private Date createdOn;
	@Column(name="messegeText")
	private String messegeText;
	@Column(name="owner")
	private String owner;

	public MessegeEB(Integer id) {
		super();
		this.id = id;
	}
	
	public MessegeEB() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public String getMessegeText() {
		return messegeText;
	}

	public void setMessegeText(String messegeText) {
		this.messegeText = messegeText;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

}
